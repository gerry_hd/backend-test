# backend-test

Django 2.1

Comandos para instalar

Instalar django:
``pip install django``

Generar tablas, correr migraciones:
``python manage.py migrate``

Ejecutar servidor:
``python manage.py runserver``

Servicios web:

_/shorten/_
: Acepta un JSON con una llave "url" que contenga una URL válida, devuelve la url acortada. Ejemplo:

``{url: "http://facebook.com"}`` => ``{shorty_url: "http://hostname/C"}``

_/shorten_many/_
: Acepta un JSON con una llave "urls" que contenga un arreglo de URLs válidas, devuelve un arreglo de URLs acortadas. Ejemplo:

``{urls: ["http://facebook.com", "http://twitter.com"]}`` => ``{shorty_urls: ["http://hostname/C","http://hostname/Fg"]}``

_/shorten_file/_
: Acepta un archivo de texto con una URL válida por línea. Devuelve las URL acortadas en el mismo orden.

_/<<url_acortada>>/_
: Acepta el contenido de "url acordata", lo decodifica de base62 y si se encuentra como una ID, redirecciona a la URL correspondiente.