from django.db import models
import modules.base62 as base62

# Create your models here.
class Link(models.Model):
    """
    Model for shortened url
    """
    url = models.URLField(max_length=2000)
    visits = models.IntegerField(default=0)

    def to_base62(self):
        return base62.encode(self.id)

    def __str__(self):
        return self.url