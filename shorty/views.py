from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.core.exceptions import ValidationError
from django.db.models import F
from shorty.models import Link
from io import StringIO


import modules.base62 as base62
import json

# Create your views here.

def shorten(request):
    if request.method == "POST" and request.body:
        json_data = json.loads(request.body)
        link = Link()
        link.url = json_data["url"]
        try:
            link.full_clean()
            link.save()
            shorty = request.scheme + "://" + request.get_host() + "/" + link.to_base62()
            return JsonResponse({'shorty_url': shorty})
        except Exception as ex:
            return HttpResponse(str(ex))

def shorten_many(request):
    if request.method == "POST" and request.body:
        json_data = json.loads(request.body)
        links = json_data["urls"]
        shorty_urls = []
        for l in links:
            link = Link()
            link.url = l
            try:
                link.full_clean()
                link.save()
                shorty_url = request.scheme + "://" + request.get_host() + "/" + link.to_base62()
            except ValidationError:
                shorty_url = "Invalid URL format: " + link.url

            shorty_urls.append(shorty_url)
        return JsonResponse({"shorty_urls": shorty_urls})

def shorten_file(request):
    if request.method == 'POST' and request.FILES['urlsfile']:
        urls_file = request.FILES['urlsfile']
        f = urls_file.open()
        lines = f.readlines()
        shorty_file = StringIO()
        for line in lines:
            link = Link()
            link.url = line.decode('utf-8').rstrip("\n")
            try:
                link.full_clean()
                link.save()
                shorty_url = request.scheme + "://" + request.get_host() + "/" + link.to_base62()
            except ValidationError:
                shorty_url = "Invalid URL format: " + link.url
            shorty_file.write(shorty_url+"\n")
        response = HttpResponse(shorty_file.getvalue(), content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=shorty_urls.txt'
        return response


def follow(request, base62_id):
    if request.method == "GET":
        link_id = base62.decode(base62_id)
        link = get_object_or_404(Link, id=link_id)
        link.visits = F('visits') + 1
        link.save()
        return redirect(link.url)

def index(request):
    return render(request, 'shorty_ui.html')

def all(request):
    base62_links = []
    for link in Link.objects.all():
        base62_links.append(request.scheme + "://" + request.get_host() + "/" + link.to_base62())

    context = {
        "urls": Link.objects.all(),
        "shorty_urls": base62_links
    }
    return render(request, 'all.html', context)


