from django.urls import path

from . import views

urlpatterns = [
    path('shorten/', views.shorten, name='shorten'),
    path('all/', views.all, name='all'),
    path('shorten_many/', views.shorten_many, name='shorten_many'),
    path('shorten_file/', views.shorten_file, name='shorten_file'),
    path('<slug:base62_id>/', views.follow, name="follow"),
    path('', views.index, name="index")

]